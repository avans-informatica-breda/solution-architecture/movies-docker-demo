# 2 the Movies - an example Docker project

This is a small example of three parts of a system that are containerized into Docker containers.

## Installing and starting

To install:

### Docker

To build and run the backend nodeserver and frontend Angular app as separate containers, see the Readme's in the corresponding folders.

### Docker Compose

Docker-compose starts the containers in this project all from one script.

Since we use a database, the required database, tables and dummy data needs to be installed into the container database instance. The MySql database that we use installs SQL scripts as first installation from a given folder. This folder is linked as a volume into the mysql container. Only at very first startup this script is read. Prune your images until it is read.

To clear all images and restart all over, so that the SQL script is installed:

```
docker system prune -a
```

Then bring the system up with a fresh build.

```
docker-compose up --build
```

## Further reading

- [Docker Getting Started](https://docs.docker.com/get-started/overview/)
- [How To Remove Docker Images, Containers, and Volumes](https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes)
