import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { DashboardComponent } from './components/dashboard/dashboard.component'
import { HeaderComponent } from './components/header/header.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AlertModule } from './modules/alert/alert.module'
import { AlertService } from './modules/alert/alert.service'
import { HttpClientModule } from '@angular/common/http'
import { ServerSelectorModule } from './components/serverselector/serverselector.module'
import { InfoComponent } from './components/info/info.component'

@NgModule({
  declarations: [AppComponent, DashboardComponent, HeaderComponent, InfoComponent],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AlertModule,
    ServerSelectorModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [AlertService],
  bootstrap: [AppComponent]
})
export class AppModule {}
