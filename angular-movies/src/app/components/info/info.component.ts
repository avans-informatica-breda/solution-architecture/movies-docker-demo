import { Component } from '@angular/core'

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html'
})
export class InfoComponent {
  info = {
    Student_Name: 'Jouw Naam',
    Student_Nr: 1234567,
    Description: 'Some description',
    Sonarqube_Url: 'http://hier.de.url'
  }

  studenthomes = {
    results: [
      {
        ID: 1,
        Name: 'Het studentenhuis',
        Address: 'Straatnaam',
        House_Nr: 12,
        Postal_Code: '4834QQ',
        City: 'Breda',
        Telephone: '061234567891',
        UserID: 1,
        ImageUrl: 'Optional',
        ImagePath: 'Optional'
      }
    ]
  }

  studenthome = {
    results: {
      ID: 1,
      Name: 'Het studentenhuis',
      Address: 'Straatnaam',
      House_Nr: 12,
      Postal_Code: '4834QQ',
      City: 'Breda',
      Telephone: '061234567891',
      UserID: 1,
      ImageUrl: 'Optional',
      ImagePath: 'Optional'
    }
  }
}
