/**
 *
 */
export class ServerSelector {
  id: number = undefined
  name: string
  releaseyear: number
  studio: string
  url: string

  constructor(values: any = {}) {
    // Assign all values to this objects properties
    Object.assign(this, values)
  }
}
