import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { ServerSelectorsComponent } from './serverselector.component'
import { ServerSelectorListComponent } from './serverselector-list/serverselector-list.component'

const routes: Routes = [
  {
    path: 'server',
    component: ServerSelectorsComponent,
    children: [{ path: '', component: ServerSelectorListComponent }]
  }
]

@NgModule({
  imports: [
    // Always use forChild in child route modules!
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ServerSelectorRoutingModule {}
