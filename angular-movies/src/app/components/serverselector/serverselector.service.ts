import { Injectable } from '@angular/core'
import { Observable, BehaviorSubject, of } from 'rxjs'
import { map, tap, catchError } from 'rxjs/operators'
import { ServerSelector } from './serverselector.model'
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ServerSelectorService {
  servers: ServerSelector[] = []

  public selectedServer$ = new BehaviorSubject<ServerSelector>(undefined)

  constructor(private http: HttpClient) {}

  public getMovies(): Observable<ServerSelector[]> {
    const url = `${environment.apiUrl}/api/movie`
    console.log('getMovies', url)
    return this.http.get<ApiResponse>(url).pipe(
      map((data: any) => data.result),
      tap(console.log),
      catchError(this.handleError)
    )
  }

  private handleError(error: HttpErrorResponse) {
    console.log('handleError')
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message)
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      // console.error(`Backend returned code ${error.status}, ` + `body was: ${error.message}`)

      return of({ error: 'Cannot connect' })
    }
  }
}

/**
 * This interface specifies the structure of the expected API server response.
 */
export interface ApiResponse {
  result: any[]
}
