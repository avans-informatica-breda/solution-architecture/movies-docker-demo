import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ServerSelectorListComponent } from './serverselector-list/serverselector-list.component'
import { ServerSelectorRoutingModule } from './serverselector-routing.module'
import { ServerSelectorsComponent } from './serverselector.component'
import { ServerSelectorItemComponent } from './serverselector-list/serverselector-item/serverselector-item.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { HttpClientModule } from '@angular/common/http'

@NgModule({
  declarations: [ServerSelectorsComponent, ServerSelectorItemComponent, ServerSelectorListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    ServerSelectorRoutingModule
  ],
  providers: [],
  exports: [ServerSelectorsComponent, ServerSelectorListComponent]
})
export class ServerSelectorModule {}
