import { Component, OnInit, OnDestroy } from '@angular/core'
import { Router } from '@angular/router'
import { ServerSelector } from '../serverselector.model'
import { ServerSelectorService } from '../serverselector.service'
import { AlertService } from '../../../modules/alert/alert.service'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-serverselector-list',
  templateUrl: './serverselector-list.component.html'
})
export class ServerSelectorListComponent implements OnInit, OnDestroy {
  title = 'Movies'
  serverselectors: ServerSelector[]
  subs: Subscription

  constructor(
    private alertService: AlertService,
    private router: Router,
    private serverselectorService: ServerSelectorService
  ) {}

  ngOnInit() {
    this.subs = this.serverselectorService.getMovies().subscribe(
      (serverselectors) => (this.serverselectors = serverselectors),
      (error) => {
        console.log(error)
        this.alertService.error(
          '<strong>Connection error:</strong> ' + error + '<br/>(Is de server bereikbaar?)'
        )
      }
    )
  }

  ngOnDestroy() {
    if (this.subs) {
      this.subs.unsubscribe()
    }
  }
}
