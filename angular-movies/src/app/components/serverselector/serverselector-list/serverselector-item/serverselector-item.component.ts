import { Component, Input } from '@angular/core'
import { ServerSelector } from '../../serverselector.model'

@Component({
  // tslint:disable-next-line: component-selector
  selector: '[app-serverselector-item]',
  templateUrl: './serverselector-item.component.html'
})
export class ServerSelectorItemComponent {
  @Input() serverSelector: ServerSelector
}
