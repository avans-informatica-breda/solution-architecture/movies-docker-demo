# 2 The Movies Angular frontend

Angular frontend at the movies backend server.

## Requirements

- nodejs and npm
- the node-movies-server connected to a running instance of MySql.

## Usage

Fork this repo and clone your copy onto your local machine.

### Run on local machine

To run this frondend on your machine, non-containerized and in development mode, run

```
npm install
ng serve
```

### Run in Docker

To run this frontend as a containerized app and in production mode, build and run the required instance using the Dockerfile.

```
docker build --tag frontend:1.0 .
docker run --publish 4200:4200 --name frontend frontend:1.0
```

To remove the container instance, run

```
docker rm --force frontend
```
