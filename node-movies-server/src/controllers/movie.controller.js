const config = require('../config/config')
const logger = config.logger
const assert = require('assert')
const database = require('../config/database')
const pool = require('../config/database')

let controller = {
  //
  validateMovie(req, res, next) {
    try {
      const { name, releaseyear, studio } = req.body
      assert(typeof name === 'string', 'name is missing!')
      assert(typeof releaseyear === 'number', 'releaseyear is missing!')
      assert(typeof studio === 'string', 'studio is missing!')
      next()
    } catch (err) {
      res.status(400).json({
        message: 'Error!',
        error: err.toString()
      })
    }
  },

  createMovie(req, res, next) {
    logger.info('createMovie called')
    const movie = req.body
    let { name, releaseyear, studio } = movie
    logger.trace('movie =', movie)

    // !!
    const userid = req.userId // 1

    let sqlQuery =
      'INSERT INTO `movies` (`name`, `releaseyear`, `studio`, `userid`) VALUES (?, ?, ?, ?)'
    logger.debug('createMovie', 'sqlQuery =', sqlQuery)

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error('createMovie', error)
        res.status(400).json({
          message: 'createMovie failed getting connection!',
          error: err
        })
      }
      if (connection) {
        // Use the connection
        connection.query(
          sqlQuery,
          [name, releaseyear, studio, userid],
          (error, results, fields) => {
            // When done with the connection, release it.
            connection.release()
            // Handle error after the release.
            if (error) {
              logger.error('createMovie', error)
              res.status(400).json({
                message: 'createMovie failed calling query',
                error: error
              })
            }
            if (results) {
              logger.trace('results: ', results)
              res.status(200).json({
                result: {
                  id: results.insertId,
                  ...movie
                }
              })
            }
          }
        )
      }
    })
  },

  getAll(req, res, next) {
    const studioname = req.query.studio
    logger.info('getAll', 'studioname =', studioname)
    let sqlQuery =
      'SELECT * FROM movies' + (studioname ? ' WHERE studio = ?' : '')
    logger.debug('getAll', 'sqlQuery =', sqlQuery)

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error('getAll failed!', err)
        res.status(400).json({
          message: 'getAll failed!',
          error: err
        })
      }
      if (connection) {
        // Use the connection
        connection.query(sqlQuery, [studioname], (error, results, fields) => {
          // When done with the connection, release it.
          connection.release()
          // Handle error after the release.
          if (error) {
            res.status(400).json({
              message: 'GetAll failed!',
              error: error
            })
          }
          if (results) {
            logger.trace('results: ', results)
            res.status(200).json({
              result: results
            })
          }
        })
      }
    })
  },

  getById(req, res, next) {
    const movieId = req.params.movieId

    logger.info('Get aangeroepen op /api/movie/', movieId)
    res.status(200).json({
      result: 'Not implemented yet'
    })
  },

  deleteById(req, res, next) {
    const id = req.params.movieId
    const userid = req.userId
    logger.debug('delete', 'id =', id, 'userid =', userid)

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          message: 'delete failed!',
          error: err
        })
      }

      // Use the connection
      let sqlQuery = 'DELETE FROM movies WHERE id = ? AND UserID = ?'
      connection.query(sqlQuery, [id, userid], (error, results, fields) => {
        // When done with the connection, release it.
        connection.release()
        // Handle error after the release.
        if (error) {
          res.status(400).json({
            message: 'Could not delete item',
            error: error
          })
        }
        if (results) {
          if (results.affectedRows === 0) {
            logger.trace('item was NOT deleted')
            res.status(401).json({
              result: {
                error: 'Item not found of you do not have access to this item'
              }
            })
          } else {
            logger.trace('item was deleted')
            res.status(200).json({
              result: 'success'
            })
          }
        }
      })
    })
  }
}

module.exports = controller
