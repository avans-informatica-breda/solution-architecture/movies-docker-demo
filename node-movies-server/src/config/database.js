const mysql = require('mysql')
const logger = require('./config').logger
const dbconfig = require('./config').dbconfig

logger.trace('dbconfig', dbconfig)
const pool = mysql.createPool(dbconfig)

pool.on('connection', function (connection) {
  logger.trace('Database connection established')
})

pool.on('acquire', function (connection) {
  logger.trace('Database connection aquired')
})

pool.on('release', function (connection) {
  logger.trace('Database connection released')
})

module.exports = pool

//
// Onderstaande code implementeert een 'fake' database op basis van een array.
// Deze database maak je in de eerste twee lessen zodat je kunt
// oefenen met JavaScript en callback functions.
// Deze database wordt dan via de functions gebruikt in de controller.
//
// module.exports = {
//   database: [],
//   lastInsertId: 0,

//   addMovie(movie, callback) {
//     setTimeout(() => {
//       logger.info('addMovie called')
//       const insertedMovie = {
//         id: this.lastInsertId++,
//         ...movie
//       }
//       this.database.push(insertedMovie)
//       logger.info('Movie added to the database')
//       callback(undefined, {
//         movie: insertedMovie
//       })
//     }, 300)
//   },

//   getAll(callback) {
//     setTimeout(() => {
//       logger.info('getAll called')
//       callback(undefined, { rows: this.database })
//     }, 300)
//   },

//   delete(id, callback) {
//     setTimeout(() => {
//       logger.info('delete called')
//       const moviesToFind = this.database.filter((item) => item.id == id)
//       if (moviesToFind.length === 0) {
//         logger.info('id not found')
//         callback({ error: 'id not found' }, undefined)
//       } else {
//         const movieToFind = moviesToFind[0]
//         const filteredResult = this.database.filter((item) => item.id != id)
//         this.database = filteredResult
//         logger.info('movie deleted')
//         callback(undefined, { rows: movieToFind })
//       }
//     }, 300)
//   }
// }
