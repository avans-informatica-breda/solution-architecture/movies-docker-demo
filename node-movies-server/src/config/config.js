const loglevel = process.env.LOGLEVEL || 'trace'

module.exports = {
  dbconfig: {
    host: process.env.DB_HOST || 'localhost',
    user: process.env.DB_USER || 'movies_user',
    database: process.env.DB_DATABASE || 'movies',
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT || 3306,
    connectionLimit: 10
  },

  logger: require('tracer').console({
    format: ['{{timestamp}} [{{title}}] {{file}}:{{line}} : {{message}}'],
    preprocess: function (data) {
      data.title = data.title.toUpperCase()
    },
    dateformat: 'isoUtcDateTime',
    level: loglevel
  })
}
