process.env.DB_DATABASE = process.env.DB_DATABASE || 'movies_testdb'
process.env.NODE_ENV = 'testing'
console.log(`Running tests using database '${process.env.DB_DATABASE}'`)

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../index')
const pool = require('../../src/config/database')
const jwt = require('jsonwebtoken')

chai.should()
chai.use(chaiHttp)

const CLEAR_MOVIES_TABLE = 'DELETE IGNORE FROM `movies`'
const INSERT_MOVIE =
  'INSERT INTO `movies` (`name`, `releaseyear`, `studio`, `userid`) VALUES ' +
  "('Finding Nemo', 2003, 'Pixar', 1)," +
  "('Finding Nemo', 2003, 'Pixar', 1)"
const INSERT_USER =
  'INSERT INTO `user` (ID, `First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES' +
  '(1, "Jan", "Smit", "jsmit@server.nl","222222", "secret");'

before((done) => {
  pool.query(INSERT_USER, (err, rows, fields) => {
    if (err) {
      console.log(`before INSERT_USER: ${err}`)
      done(err)
    } else {
      done()
    }
  })
})

beforeEach((done) => {
  pool.query(CLEAR_MOVIES_TABLE, (err, rows, fields) => {
    if (err) {
      console.log(`beforeEach CLEAR_MOVIES_TABLE: ${err}`)
      done(err)
    } else {
      done()
    }
  })
})

after((done) => {
  pool.query(CLEAR_MOVIES_TABLE, (err, rows, fields) => {
    if (err) {
      console.log(`after error: ${err}`)
      done(err)
    } else {
      done()
    }
  })
})

describe('Manage movies', () => {
  describe('UC201 Create movie - POST /api/movie', () => {
    it('TC-201-1 should return valid error when required value is not present', (done) => {
      chai
        .request(server)
        .post('/api/movie')
        .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
        .send({
          releaseyear: 1234,
          studio: 'a studioname'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.should.be.an('object')

          let { message, error } = res.body
          message.should.be.an('string').that.equals('Error!')
          error.should.be.an('string')

          done()
        })
    })

    it('TC-201-2 should return a valid error when postal code is invalid', (done) => {
      done()
    })

    it('TC-201-6 should successfully add an item when posting valid values', (done) => {
      jwt.sign({ id: 1 }, 'secret', { expiresIn: '2h' }, (err, token) => {
        chai
          .request(server)
          .post('/api/movie')
          .set('authorization', 'Bearer ' + token)
          .send({
            name: 'a name',
            releaseyear: 1234,
            studio: 'a studioname'
          })
          .end((err, res) => {
            res.should.have.status(200)
            res.body.should.be.an('object').that.has.property('result')

            let result = res.body.result
            result.should.be
              .an('object')
              .that.has.property('id')
              .that.is.a('number')
            done()
          })
      })
    })
  })

  describe('UC202 List movies - GET /api/movie', () => {
    it('TC-202-1 should return empty list when database contains no items', (done) => {
      chai
        .request(server)
        .get('/api/movie')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body
          result.should.be.an('array').that.has.length(0)

          done()
        })
    })

    it('TC-202-2 should show 2 results', (done) => {
      pool.query(INSERT_MOVIE, (error, result) => {
        if (error) console.log(error)
        if (result) {
          chai
            .request(server)
            .get('/api/movie')
            .end((err, res) => {
              res.should.have.status(200)
              res.should.be.an('object')

              let { result } = res.body
              result.should.be.an('array').that.has.length(2)

              let { id, name, releaseyear, studio } = result[0]
              name.should.be.a('string').that.equals('Finding Nemo')
              releaseyear.should.be.a('number').that.equals(2003)
              studio.should.be.a('string').that.equals('Pixar')
              id.should.be.a('number').that.is.at.least(0)

              done()
            })
        }
      })
    })
  })
})
