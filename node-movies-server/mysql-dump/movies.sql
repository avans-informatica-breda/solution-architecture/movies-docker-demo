DROP DATABASE IF EXISTS `movies`;
CREATE DATABASE `movies`;
-- DROP DATABASE IF EXISTS `movies_testdb`;
-- CREATE DATABASE `movies_testdb`;

-- -- --
-- -- -- Uncomment de volgende SQL statements om een user in de database te maken
-- -- -- Vanwege security mag je die user alleen in je lokale ontwikkeldatabase aanmaken!
-- -- -- Op een remote 'productie'-server moet je zorgen voor een ANDER useraccount!
-- -- -- Vanuit je (bv. nodejs) applicatie stel je de credentials daarvan in via environment variabelen.
-- -- --
-- -- -- movies_user aanmaken
CREATE USER 'movies_user'@'%' IDENTIFIED BY 'secret';
CREATE USER 'movies_user'@'localhost' IDENTIFIED BY 'secret';

-- geef rechten aan deze user
GRANT SELECT, INSERT, DELETE, UPDATE ON `movies`.* TO 'movies_user'@'%';
-- GRANT SELECT, INSERT, DELETE, UPDATE ON `movies_testdb`.* TO 'movies_user'@'%';

USE `movies`;

-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user` ;
CREATE TABLE IF NOT EXISTS `user` (
	`ID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`First_Name` VARCHAR(32) NOT NULL,
	`Last_Name` VARCHAR(32) NOT NULL,
	`Email` VARCHAR(32) NOT NULL UNIQUE,
	`Student_Number` VARCHAR(32) NOT NULL,
	`Password` CHAR(64) BINARY NOT NULL,
	PRIMARY KEY (`ID`)
)
ENGINE = InnoDB;

-- Voorbeeld insert query. Wanneer je in Nodejs de ? variant gebruikt hoeven de '' niet om de waarden.
-- Zet die dan wel in het array er na, in de goede volgorde.
-- In je Nodejs app zou het password wel encrypted moeten worden.
INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password` ) VALUES
('Jan', 'Smit', 'jsmit@server.nl','222222', 'secret');

-- -----------------------------------------------------
-- Table `movies`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `movies` ;
CREATE TABLE IF NOT EXISTS `movies` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(32) NOT NULL,
	`releaseyear` INT UNSIGNED,
	`studio` VARCHAR(32) NOT NULL,
	`userid` INT UNSIGNED NOT NULL,
	PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

ALTER TABLE `movies`
ADD CONSTRAINT `fk_movies_user`
FOREIGN KEY (`userid`) REFERENCES `user` (`id`)
ON DELETE NO ACTION
ON UPDATE CASCADE;


-- Voorbeeld insert query. Wanneer je in Nodejs de ? variant gebruikt hoeven de '' niet om de waarden.
INSERT INTO `movies` (`name`, `releaseyear`, `studio`, `userid`) VALUES
('Finding Docker', 2003, 'Pixar', 1),
('Need for Docker', 2015, 'Paramount', 1),
('Docker Runner', 1982, 'Warner Brothers Pictures', 1);
